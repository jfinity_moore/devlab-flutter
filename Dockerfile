FROM registry.gitlab.com/jfinity_moore/code-server

USER root
RUN apt-get update && apt-get install -y \
	lib32stdc++6 \
	unzip

USER coder
WORKDIR /home/coder
ENV PATH="${PATH}:/home/coder/flutter/bin:/home/coder/flutter/bin/cache/dart-sdk/bin:/home/coder/flutter/.pub-cache/bin:/home/coder/.pub-cache/bin"

RUN curl -SL https://storage.googleapis.com/flutter_infra/releases/stable/linux/flutter_linux_v1.5.4-hotfix.2-stable.tar.xz \
	| tar -xJC . \
	&& flutter config --no-analytics

RUN flutter precache --all-platforms



RUN mkdir -p ./.local/share/code-server/User/ ./templates/ \
	&& code-server --install-extension dart-code.flutter \
	&& code-server --install-extension dart-code.dart-code

COPY ./config/Code/User/settings.json ./.local/share/code-server/User/
COPY ./config/flutter_web/pubspec.yaml ./templates/

RUN (cd ./templates && pub global activate webdev && pub upgrade)



WORKDIR /home/coder/project

EXPOSE 8443
EXPOSE 8080

ENTRYPOINT ["dumb-init", "code-server"]

# docker run -d --name devlab -p 127.0.0.1:8443:8443 -p 8800:8080 -v "${PWD}:/home/coder/project" registry.gitlab.com/jfinity_moore/devlab-flutter --allow-http --no-auth
